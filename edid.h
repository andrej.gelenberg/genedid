#ifndef GEDID_H
#define GEDID_H

#include <stdint.h>

#ifndef _EDID_C
extern uint8_t edid_magick[8];
#endif /* _EDID_C */

enum {
  EDID_VER = 1,
  EDID_REV = 3,

  EDID_DETTIMING_ANALOG = 0x00,
  EDID_DETTIMING_BIPOLAR= 0x08,
  EDID_DETTIMING_DIGCOMP= 0x10,
  EDID_DETTIMING_DIGSEP = 0x18,

  EDID_DETTIMING_SERRATE=0x04,
  EDID_DETTIMING_VERTPOL=0x04,
  EDID_DETTIMING_ONRGB  =0x02,
  EDID_DETTIMING_COMPPOL=0x02,
  EDID_DETTIMING_HORPOL =0x02,

  EDID_STEREO_NO       = 0x00,
  EDID_STEREO_SEQRIGHT = 0x20,
  EDID_STEREO_SEQLEFT  = 0x40,
  EDID_STEREO_2INTRIGHT= 0x21,
  EDID_STEREO_2INTLEFT = 0x41,
  EDID_STEREO_4INT     = 0x60,
  EDID_STEREO_SIDEBYSIDE=0x61,
};

struct edid_manid_t {
  uint8_t a1h: 2;
  uint8_t a0 : 5;
  uint8_t    : 1;
  uint8_t a2 : 5;
  uint8_t a1l: 3;
} __attribute__((packed));
typedef struct edid_manid_t edid_manid_t;

struct edid_header_t {
  uint8_t       magick[8];
  edid_manid_t  manid;
  uint16_t      prodid;
  uint32_t      serial;
  uint8_t       week;
  uint8_t       year;
  uint8_t       ver;
  uint8_t       rev;
} __attribute__((packed));
typedef struct edid_header_t edid_header_t;

struct edid_display_param_t {
  /* VESA DFP 1.x TMDS CRGB on digital_input = 1 */
  uint8_t serrated_on_green : 1;

  uint8_t sync_on_green     : 1;
  uint8_t comp_sync         : 1;
  uint8_t sep_sync          : 1;
  uint8_t blank_to_black    : 1;
  uint8_t sync              : 2;

  uint8_t digital_input     : 1;
} __attribute__((packed));
typedef struct edid_display_param_t edid_display_param_t;

enum edid_digital_type_e {
  rgb444             = 0,
  rgb444yuv444       = 1,
  rgb444yuv422       = 2,
  rgb444yuv444yuv422 = 3
};
typedef enum edid_digital_type_e edid_digital_type_e;

enum edid_analog_type_e {
  gray   = 0,
  rgb    = 1,
  nonrgb = 2,
  undef  = 3
};
typedef enum edid_analog_type_e edid_analog_type_e;

struct edid_display_features_t {
  uint8_t             def_gtf    : 1;
  uint8_t             def_timing : 1;
  uint8_t             srgb       : 1;
  uint8_t             type       : 2;
  uint8_t             active_off : 1;
  uint8_t             suspend    : 1;
  uint8_t             standby    : 1;
} __attribute__((packed));
typedef struct edid_display_features_t edid_display_features_t;

struct edid_display_t {
  edid_display_param_t    param;
  uint8_t                 width;
  uint8_t                 height;
  uint8_t                 gamma;
  edid_display_features_t features;
} __attribute__((packed));
typedef struct edid_display_t edid_display_t;

struct edid_chroma_t {
  uint8_t green_y_low : 2;
  uint8_t green_x_low : 2;

  uint8_t red_y_low   : 2;
  uint8_t red_x_low   : 2;

  uint8_t white_y_low : 2;
  uint8_t white_x_low : 2;

  uint8_t blue_y_low  : 2;
  uint8_t blue_x_low  : 2;

  uint8_t red_x_high;
  uint8_t red_y_high;

  uint8_t green_x_high;
  uint8_t green_y_high;

  uint8_t blue_x_high;
  uint8_t blue_y_high;

  uint8_t white_x_high;
  uint8_t white_y_high;
} __attribute__((packed));
typedef struct edid_chroma_t edid_chroma_t;

struct edid_timing_bitmap_t {

  uint8_t m800x600_60  : 1;
  uint8_t m800x600_56  : 1;
  uint8_t m640x480_75  : 1;
  uint8_t m640x480_72  : 1;
  uint8_t m640x480_67  : 1;
  uint8_t m640x480_60  : 1;
  uint8_t m720x400_88  : 1;
  uint8_t m720x400_70  : 1;

  uint8_t m1280x1024_75: 1;
  uint8_t m1024x768_75 : 1;
  uint8_t m1024x768_70 : 1;
  uint8_t m1024x768_60 : 1;
  uint8_t m1024x768_87 : 1;
  uint8_t m832x624_75  : 1;
  uint8_t m800x600_75  : 1;
  uint8_t m800x600_72  : 1;

  uint8_t              : 7;
  uint8_t m1152x870_75 : 1;
} __attribute__((packed));
typedef struct edid_timing_bitmap_t edid_timing_bitmap_t;

enum aspect_ratio_e {
  r16_10 = 0,
  r4_3   = 1,
  r5_4   = 2,
  r16_9  = 3
};
typedef enum aspect_ratio_e aspect_ratio_e;

struct edid_timing_t {
  uint8_t        width;
  uint8_t        freq   : 6;
  aspect_ratio_e aspect : 2;
} __attribute__((packed));
typedef struct edid_timing_t edid_timing_t;

struct edid_dettiming_t {
  uint16_t pixel_clock;

  uint8_t  hor_act_low;
  uint8_t  hor_blank_low;
  uint8_t  hor_blank_high : 4;
  uint8_t  hor_act_high   : 4;

  uint8_t  vert_act_low;
  uint8_t  vert_blank_low;
  uint8_t  vert_blank_high : 4;
  uint8_t  vert_act_high   : 4;

  uint8_t  hor_sync_offset_low;
  uint8_t  hor_sync_pulse_low;
  uint8_t  vert_sync_pulse_low  : 4;
  uint8_t  vert_sync_offset_low : 4;
  uint8_t  vert_sync_pulse_high : 2;
  uint8_t  vert_sync_offset_high: 2;
  uint8_t  hor_sync_pulse_high  : 2;
  uint8_t  hor_sync_offset_high : 2;

  uint8_t  image_width_low;
  uint8_t  image_height_low;
  uint8_t  image_height_high: 4;
  uint8_t  image_width_high : 4;

  uint8_t  hor_border;
  uint8_t  vert_border;

  uint8_t  flags;

} __attribute__((packed));
typedef struct edid_dettiming_t edid_dettiming_t;

enum edid_desc_type_e {
  DESC_SERIAL = 0xff,
  DESC_STRING = 0xfe,
  DESC_RANGE  = 0xfd,
  DESC_NAME   = 0xfc,
  DESC_COLOR  = 0xfb,
  DESC_TIMING = 0xfa,
  DESC_DUMMY  = 0x20,
};
typedef enum edid_desc_type_e edid_desc_type_e;

struct edid_desc_secgtf_t {
  uint8_t  mark;
  uint8_t  start_freq;
  uint8_t  c;
  uint16_t m;
  uint8_t  k;
  uint8_t  j;
} __attribute__((packed));
typedef struct edid_desc_secgtf_t edid_desc_secgtf_t;

enum edid_desc_sectiming_e {
  EDID_DESC_NO_SECTIMING = 0,
  EDID_DESC_SECGTF       = 2,
};
typedef enum edid_desc_sectiming_e edid_desc_sectiming_e;

struct edid_desc_range_t {
  uint8_t               vert_freq_min;
  uint8_t               vert_freq_max;
  uint8_t               hor_freq_min;
  uint8_t               hor_freq_max;
  uint8_t               pixel_clock_max;
  edid_desc_sectiming_e second_timing_type : 8;
  union {
    struct {
      uint8_t mark0;
      uint8_t mark1[6];
    } dummy;
    edid_desc_secgtf_t secgtf;
  } second_timing_data;
} __attribute__((packed));
typedef struct edid_desc_range_t edid_desc_range_t;

struct edid_desc_color_white_t {
  uint8_t index;
  uint8_t low;
  uint8_t x;
  uint8_t y;
  uint8_t gamma;
} __attribute__((packed));
typedef struct edid_desc_color_white_t edid_desc_color_white_t;

struct edid_desc_color_t {
  edid_desc_color_white_t white[2];
  uint8_t  mark1;
  uint16_t mark2;
} __attribute__((packed));
typedef struct edid_desc_color_t edid_desc_color_t;

struct edid_mondesc_t {
  uint16_t         mark1;
  uint8_t          mark2;
  edid_desc_type_e type : 8;
  uint8_t          mark3;
  union {
    char               str[13];
    edid_desc_range_t  range;
    edid_desc_color_t  color;
    struct {
      edid_timing_t      timing[6];
      uint8_t            mark;
    } timings;
  } data;
} __attribute__((packed));
typedef struct edid_mondesc_t edid_mondesc_t;

union edid_descriptor_t {
  edid_mondesc_t   monitordesc;
  edid_dettiming_t dettiming;
};
typedef union edid_descriptor_t edid_descriptor_t;

struct edid_t {
  edid_header_t        header;
  edid_display_t       display;
  edid_chroma_t        chroma;
  edid_timing_bitmap_t timing_bitmap;
  edid_timing_t        timing[8];
  edid_dettiming_t     dettiming;
  edid_descriptor_t    descriptor[3];
  uint8_t              extblocks;
  uint8_t              checksum;
} __attribute__((packed));
typedef struct edid_t edid_t;

#endif /* GEDID_H */
