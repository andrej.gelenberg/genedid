#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "edid.h"

static const char *
prog = 0;

static const char *
sync_param[] = {
  "+0.7/−0.3 V",
  "+0.714/−0.286 V",
  "+1.0/−0.4 V",
  "+0.7/0 V"
};

static const char *
digital_display_type[] = {
  "RGB 4:4:4",
  "RGB 4:4:4 + YCrCb 4:4:4",
  "RGB 4:4:4 + YCrCb 4:2:2",
  "RGB 4:4:4 + YCrCb 4:4:4 + YCrCb 4:2:2"
};

static const char *
aspect2str[] = {
    "16:10",
    "4:3",
    "5:4",
    "16:9"
  };

static const char *
analog_display_type[] = {
  "Monochrome or Grayscale",
  "RGB color",
  "Non-RGB color",
  "Undefined"
};

const uint16_t
aspectheight(
    uint16_t       width,
    aspect_ratio_e aspect) {

  uint16_t ret;

  switch(aspect) {
    case r16_10:
      ret = width * 10 / 16;
      break;

    case r4_3:
      ret = width * 3 / 4;
      break;

    case r5_4:
      ret = width * 4 / 5;
      break;

    case r16_9:
      ret = width * 9 / 16;
      break;

    default:
      ret = 0;

  }

  return ret;

}

static double
chrom2f(
    uint8_t low,
    uint8_t high) {

  double ret = 0;

  uint32_t b = (high << 2) | low ;
  int i;
  double id = 1;
  for(i=0; i<10; ++i) {
    id *= 0.5;
    if ( b &  0b1000000000) {
      ret += id;
    }
    b <<= 1;
  }

  return ret;

}

static void
usage() {
  printf("usage: %s <edid dump file>\n", prog);
}

static const char *
bool(
    int val) {
  return val ? "true" : "false";
}

static int
print_detailed_timing(
    edid_dettiming_t *d) {

  __label__ end, err;
  int ret = 0;

  uint16_t hor_act = ((uint16_t)d->hor_act_high   << 8) | d->hor_act_low;
  uint16_t vert_act= ((uint16_t)d->vert_act_high  << 8) | d->vert_act_low;
  uint16_t hor_sync_offset =
      (((uint16_t)d->hor_sync_offset_high << 8) | d->hor_sync_offset_low);
  uint16_t vert_sync_offset =
      (((uint16_t)d->vert_sync_offset_high << 8) | d->vert_sync_offset_low);
  uint16_t hor_sync_pulse =
      ((uint16_t)d->hor_sync_pulse_high  << 8) | d->hor_sync_pulse_low;
  uint16_t vert_sync_pulse =
      ((uint16_t)d->vert_sync_pulse_high  << 8) | d->vert_sync_pulse_low;
  uint16_t hor_blank = ((uint16_t)d->hor_blank_high << 8) | d->hor_blank_low;
  uint16_t vert_blank= ((uint16_t)d->vert_blank_high<< 8) | d->vert_blank_low;

  printf(
      "\nDetailed Timing Descriptions\n"
      "  Pixel clock:                 %d.%02d MHz (%d)\n"

      "  Horizontal active:           %d\n"
      "  Horizontal blanking:         %d\n"
      "  Horizontal blank end:        %d\n"

      "  Vertical active:             %d\n"
      "  Vertical blanking:           %d\n"
      "  Vertical blank end:          %d\n"

      "  Horizontal sync offset:      %d\n"
      "  Horizontal sync pulse width: %d\n"
      "  Horizontal sync start:       %d\n"
      "  Horizontal sync end:         %d\n"

      "  Vertical sync offset:        %d\n"
      "  Vertical sync pulse width:   %d\n"
      "  Vertical sync start:         %d\n"
      "  Vertical sync end:           %d\n"

      "  Horizontal image size:       %d mm\n"
      "  Vertical image size:         %d mm\n"

      "  Horizontal border:           %d\n"
      "  Vertical border:             %d\n"
      "  Flags (0x%02x):\n",
      d->pixel_clock / 100, d->pixel_clock % 100,
      d->pixel_clock,

      hor_act,
      hor_blank,
      hor_act + hor_blank,

      vert_act,
      vert_blank,
      vert_act + vert_blank,

      hor_sync_offset,
      hor_sync_pulse,
      hor_act + hor_sync_offset,
      hor_act + hor_sync_offset + hor_sync_pulse,

      vert_sync_offset,
      vert_sync_pulse,
      vert_act + vert_sync_offset,
      vert_act + vert_sync_offset + vert_sync_pulse,

      ((uint16_t)d->image_width_high  << 8) | d->image_width_low,
      ((uint16_t)d->image_height_high << 8) | d->image_height_low,

      d->hor_border,
      d->vert_border,

      d->flags);

  if ( d->flags & 0x80 ) {
    printf("    Interlaced.\n");
  } else {
    printf("    Non-interlaced.\n");
  }

  switch (d->flags & 0x61) {
    case EDID_STEREO_NO:
    case 0x01:
      printf("    Normal display, no stereo.\n");
      break;

    case EDID_STEREO_SEQRIGHT:
      printf("    Field sequential stereo, right image when stereo sync = 1.\n");
      break;

    case EDID_STEREO_SEQLEFT:
      printf("    Field sequential stereo, left image when stereo sync = 1.\n");
      break;

    case EDID_STEREO_2INTRIGHT:
      printf("    2-way interleaved stereo, right image on even lines.\n");
      break;

    case EDID_STEREO_2INTLEFT:
      printf("    2-way interleaved stereo, left image on even lines.\n");
      break;

    case EDID_STEREO_4INT:
      printf("    4-way interleaved stereo.\n");
      break;

    case EDID_STEREO_SIDEBYSIDE:
      printf("    Side-by-Side interleaved stereo.\n");
      break;

    default:
      printf("something is wrong and this branch schould be never reached.\n");
      goto err;
  }

  switch (d->flags & 0x18) {
    case EDID_DETTIMING_ANALOG:
      printf("    Analog composite.\n");
      break;

    case EDID_DETTIMING_BIPOLAR:
      printf("    Bipolar analog composite.\n");
      break;

    case EDID_DETTIMING_DIGCOMP:
      printf("    Digital composite.\n");
      break;

    case EDID_DETTIMING_DIGSEP:
      printf("    Digital separate.\n");
      break;

    default:
      printf("something is wrong and this branch schould be never reached.\n");
      goto err;
  }

  if ( (d->flags & EDID_DETTIMING_DIGSEP) == EDID_DETTIMING_DIGSEP ) {

    if ( d->flags & EDID_DETTIMING_VERTPOL ) {
      printf("    VSync is positive.\n");
    } else {
      printf("    VSync is negative.\n");
    }

    if ( d->flags & EDID_DETTIMING_HORPOL ) {
      printf("    HSync is positive.\n");
    } else {
      printf("    HSync is negative.\n");
    }

  } else {

    if ( d->flags & EDID_DETTIMING_SERRATE ) {
      printf("    Serration (Hsync during Vsync).\n");
    } else {
      printf("    No serration (Hsync during Vsync).\n");
    }

    if ( d->flags & EDID_DETTIMING_DIGCOMP ) {

      if ( d->flags & EDID_DETTIMING_COMPPOL ) {
        printf("    HSync positive outside of VSync.\n");
      } else {
        printf("    HSync positive outside of VSync.\n");
      }

    } else {

      if ( d->flags & EDID_DETTIMING_ONRGB ) {
        printf("    Sync on RGB lines.\n");
      } else {
        printf("    Sync only on green line.\n");
      }

    }

  }

  puts("");

end:
  return ret;

err:
  ret = 1;
  goto end;

}

static void
print_standard_timing(
    edid_timing_t *timing) {
  uint16_t width = *((uint16_t*)timing);
  if ( width != 0x0101 ) {
    width = (timing->width + 31) * 8;
    printf(
        "  Width:        %d (%d)\n"
        "  Heighgt:      %d\n"
        "  Aspect ratio: %s\n"
        "  Frequency:    %d\n\n",
        width, timing->width,
        aspectheight(width, timing->aspect),
        aspect2str[timing->aspect],
        60 + timing->freq);
  }
}

static uint8_t
checksum_check(
    edid_t *edid) {
  uint8_t sum = 0;
  uint8_t *i = (uint8_t*)edid;
  uint8_t *e = (uint8_t*)(edid+1);
  for(; i<e; ++i) { sum += *i; }
  return sum;
}

static void
print_desc_color(
    edid_desc_color_white_t *color) {
  printf(
      "Color point:\n"
      "  White point index: %d\n"
      "  White low bits:    %d\n"
      "  White x:           %d\n"
      "  White y:           %d\n"
      "  White gamma:       %d\n",
      color->index,
      color->low,
      color->x,
      color->y,
      color->gamma);
}

static void
putstr(
    const char *str) {

  char out[14];
  char *i, *e;

  for(i=out, e=&(out[sizeof(out)]);
      i < e; ++i, ++str) {
    if ( !*str || (*str == 0x0a) ) {
      break;
    }
    *i = *str;
  }
  *i = 0;

  fputs(out, stdout);

}

static int
print_edid(
    edid_t *edid) {
  __label__ end, err;

  int ret = 0;
  int i;

  if ( *((uint64_t*)edid->header.magick) != *((uint64_t*)edid_magick) ) {
    printf("magick dosen't match!\n");
  }

  if ( checksum_check(edid) ) {
    printf("checksum failed!\n");
  }

  if ( (edid->header.ver != 1) ||
       (edid->header.rev != 3) ) {
    printf(
        "unsupported edid version %d.%d\n",
        edid->header.ver, edid->header.rev);
  }

  printf(
      "EDID Header:\n"
      "  Manufacturer ID: %c%c%c (%d %d %d) (0x%02x%02x)\n"
      "  Product ID:      %d\n"
      "  Serial number:   %d\n"
      "  Week:            %d\n"
      "  Year:            %d (%d)\n"
      "  EDID Version:    %d.%d\n"
      "\nDisplay Parameters:\n",
      'A' - 1 + edid->header.manid.a0,
      'A' - 1 + ((edid->header.manid.a1h << 3) | edid->header.manid.a1l),
      'A' - 1 + edid->header.manid.a2,
      edid->header.manid.a0,
      edid->header.manid.a1h << 3 | edid->header.manid.a1l,
      edid->header.manid.a2,
      *((uint8_t*)&(edid->header.manid)),
      *(((uint8_t*)&(edid->header.manid)) + 1),
      edid->header.prodid,
      edid->header.serial,
      edid->header.week,
      1990 + edid->header.year, edid->header.year,
      edid->header.ver, edid->header.rev);

    if ( edid->display.param.digital_input ) {
      printf(
          "  Type: digital\n"
          "  VESA DFP 1.x TMDS CRGB: %s\n",
          bool(edid->display.param.serrated_on_green));

      if ((edid->display.param.sync           != 0) ||
          (edid->display.param.blank_to_black != 0) ||
          (edid->display.param.sep_sync       != 0) ||
          (edid->display.param.comp_sync      != 0) ||
          (edid->display.param.sync_on_green  != 0)) {
        printf("display param bits 6-1 should be 0, but one is not\n");
      }
    } else {
      printf(
          "  Type: analog\n"
          "  Sync: %s\n"
          "  Blank-to-black setup (pedestal) expected: %s\n"
          "  Separate sync supported: %s\n"
          "  Composite sync (on HSync) supported: %s\n"
          "  Sync on green supported: %s\n"
          "  VSync pulse must be serrated when composite or sync-on-green is used: %s\n",
          sync_param[edid->display.param.sync],
          bool(edid->display.param.blank_to_black),
          bool(edid->display.param.sep_sync),
          bool(edid->display.param.comp_sync),
          bool(edid->display.param.sync_on_green),
          bool(edid->display.param.serrated_on_green));
    }

    printf(
        "  Image width: %d cm\n"
        "  Image height: %d cm\n"
        "  Gamma: %.10f (%d)\n"
        "  DPMS support:\n"
        "    standby:    %s\n"
        "    suspend:    %s\n"
        "    active-off: %s\n"
        ,
        edid->display.width,
        edid->display.height,
        (double)(edid->display.gamma + 100) / 100.0, edid->display.gamma,
        bool(edid->display.features.standby),
        bool(edid->display.features.suspend),
        bool(edid->display.features.active_off));

    if ( edid->display.param.digital_input ) {
      printf(
          "  Display type: %s\n",
          digital_display_type[edid->display.features.type]);
    } else {
      printf(
          "  Display type: %s\n",
          analog_display_type[edid->display.features.type]);
    }

    printf(
         "  sRGB: %s\n"
         "  preferred timing mode includes native pixel format and refresh rate: %s\n"
         "  default gtf: %s\n"
         "\nChromaticity coordinates:\n"
         "  red:\n"
         "    x: %.10f (0x%x 0x%x)\n"
         "    y: %.10f (0x%x 0x%x)\n"
         "  green:\n"
         "    x: %.10f (0x%x 0x%x)\n"
         "    y: %.10f (0x%x 0x%x)\n"
         "  blue:\n"
         "    x: %.10f (0x%x 0x%x)\n"
         "    y: %.10f (0x%x 0x%x)\n"
         "  white:\n"
         "    x: %.10f (0x%x 0x%x)\n"
         "    y: %.10f (0x%x 0x%x)\n"
         "\nTiming Bitmap:\n",
         bool(edid->display.features.srgb),
         bool(edid->display.features.def_timing),
         bool(edid->display.features.def_gtf),

         chrom2f(edid->chroma.red_x_low,   edid->chroma.red_x_high),
         edid->chroma.red_x_high, edid->chroma.red_x_low,

         chrom2f(edid->chroma.red_y_low,   edid->chroma.red_y_high),
         edid->chroma.red_y_high, edid->chroma.red_y_low,

         chrom2f(edid->chroma.green_x_low, edid->chroma.green_x_high),
         edid->chroma.green_x_high, edid->chroma.green_x_low,

         chrom2f(edid->chroma.green_y_low, edid->chroma.green_y_high),
         edid->chroma.green_y_high, edid->chroma.green_x_low,

         chrom2f(edid->chroma.blue_x_low,  edid->chroma.blue_x_high),
         edid->chroma.blue_x_high, edid->chroma.blue_x_low,

         chrom2f(edid->chroma.blue_y_low,  edid->chroma.blue_y_high),
         edid->chroma.blue_y_high, edid->chroma.blue_y_low,

         chrom2f(edid->chroma.white_x_low, edid->chroma.white_x_high),
         edid->chroma.white_x_high, edid->chroma.white_x_low,

         chrom2f(edid->chroma.white_y_low, edid->chroma.white_y_high),
         edid->chroma.white_y_high, edid->chroma.white_y_low);

#define print_timing(w, h, f) \
    if ( edid->timing_bitmap.m ## w ## x ## h ## _ ## f ) { \
      printf("  " #w "x" #h "@" #f "\n"); \
    }

    print_timing(720, 400, 70);
    print_timing(720, 400, 88);

    print_timing(640, 480, 60);
    print_timing(640, 480, 67);
    print_timing(640, 480, 72);
    print_timing(640, 480, 75);

    print_timing(800, 600, 56);
    print_timing(800, 600, 60);
    print_timing(800, 600, 72);
    print_timing(800, 600, 75);

    print_timing(832, 624, 75);

    print_timing(1024, 768, 87);
    print_timing(1024, 768, 60);
    print_timing(1024, 768, 70);
    print_timing(1024, 768, 75);

    print_timing(1280, 1024, 75);

    print_timing(1152, 870, 75);

#undef print_timing

  printf("\nStandard timing information:\n");

  for(i=0; i<8 && (edid->timing[i].width > 1) ; ++i) {
    print_standard_timing(&(edid->timing[i]));
  }

  if ( print_detailed_timing(&(edid->dettiming)) ) {
    goto err;
  }

  for(i=0; i < sizeof(edid->descriptor)/sizeof(edid->descriptor[0]); ++i) {

    if ( (edid->descriptor[i].monitordesc.mark1 == 0) &&
         (edid->descriptor[i].monitordesc.mark2 == 0) &&
         (edid->descriptor[i].monitordesc.mark3 == 0) ) {

      switch(edid->descriptor[i].monitordesc.type) {

        case DESC_SERIAL:
          fputs("Serial number: '", stdout);
          putstr(edid->descriptor[i].monitordesc.data.str);
	  puts("'");
          break;

        case DESC_STRING:
          fputs("String: '", stdout);
          putstr(edid->descriptor[i].monitordesc.data.str);
	  puts("'");
          break;

        case DESC_NAME:
          fputs("Monitor name: '", stdout);
          putstr(edid->descriptor[i].monitordesc.data.str);
	  puts("'");
          break;

        case DESC_RANGE:
          printf(
              "Range limits: \n"
              "  Minimal vertical frequency:    %3d  Hz\n"
              "  Maximum vertical frequency:    %3d  Hz\n"
              "  Minimal horizontal frequency:  %3d kHz\n"
              "  Maximum horizontal frequency:  %3d kHz\n"
              "  Maximal supported pixel clock: %3d MHz (%d)\n",
              edid->descriptor[i].monitordesc.data.range.vert_freq_min,
              edid->descriptor[i].monitordesc.data.range.vert_freq_max,
              edid->descriptor[i].monitordesc.data.range.hor_freq_min,
              edid->descriptor[i].monitordesc.data.range.hor_freq_max,
              edid->descriptor[i].monitordesc.data.range.pixel_clock_max * 10,
              edid->descriptor[i].monitordesc.data.range.pixel_clock_max);

          switch ( edid->descriptor[i].monitordesc.data.range.second_timing_type ) {

            case EDID_DESC_SECGTF:
              printf(
                  "  Secondary GTF: hor. freq: %d, C = %d, M = %d, K = %d, J = %d\n",
                  edid->descriptor[i].monitordesc.data.range.second_timing_data.secgtf.start_freq,
                  edid->descriptor[i].monitordesc.data.range.second_timing_data.secgtf.c,
                  edid->descriptor[i].monitordesc.data.range.second_timing_data.secgtf.m,
                  edid->descriptor[i].monitordesc.data.range.second_timing_data.secgtf.k,
                  edid->descriptor[i].monitordesc.data.range.second_timing_data.secgtf.j);
              break;

            default:
              printf("  Unsupported secondary timing formula\n");
            case EDID_DESC_NO_SECTIMING:
              ;

          }
          break;

        case DESC_COLOR:
          print_desc_color(
              &(edid->descriptor[i].monitordesc.data.color.white[0]));

          if ( edid->descriptor[i].monitordesc.data.color.white[0].index ) {

            print_desc_color(
                &(edid->descriptor[i].monitordesc.data.color.white[1]));

          }
          break;

        case DESC_TIMING:
          printf(
              "Standard timing:\n");
          for(i=0;
              i < sizeof(edid->descriptor[i].monitordesc.data.timings.timing) /
                  sizeof(edid->descriptor[i].monitordesc.data.timings.timing[0]);
              ++i) {

            print_standard_timing(
                 &(edid->descriptor[i].monitordesc.data.timings.timing[i]));
          }

          break;

        case DESC_DUMMY:
          break;

        default:
          printf(
              "Unknown description block: 0x%02x\n",
              edid->descriptor[i].monitordesc.type);
      }

    } else {
      print_detailed_timing(&(edid->descriptor[i].dettiming));
    }

  }

  printf("\nExtention blocks: %d\n", edid->extblocks);

end:
  return ret;
err:
  ret = 1;
  goto end;
}

int
main(
    int   argc,
    char *argv[]) {

  __label__ end, end_close, err;
  int ret = 0;
  edid_t edid;
  int fd = -1;
  ssize_t l;

  prog = argv[0];

  if ( argc < 2 ) {
    usage();
    goto end;
  }

  fd = open(argv[1], O_RDONLY);

  if ( fd == -1 ) {
    printf("can't open file '%s': %m\n", argv[1]);
    goto err;
  }

  l = read(fd, &edid, sizeof(edid));

  if ( l == -1 ) {
    printf("can't read file '%s': %m\n", argv[1]);
    goto err;
  }

  if ( l >= sizeof(edid) ) {
    ret = print_edid(&edid);
  } else {
    printf("no enough data\n");
    goto err;
  }

end_close:
  close(fd);
end:
  return ret;

err:
  ret = 1;
  goto end_close;
}
