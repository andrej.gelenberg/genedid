#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "edid.h"

static uint8_t
gam(
    float g) {

  return g * 100 - 100;
}

static void
setstr(
    char *dst,
    const char *src) {

  char *e = dst + 12;

  for(; (dst < e) && (*src); ++dst, ++src) {
    *dst = *src;
  }

  *dst = 0xa;
  ++dst;

  for(; dst <= e; ++dst) {
    *dst = 0x20;
  }

}

static uint8_t
checksum(
    uint8_t *d,
    size_t len) {

  uint8_t ret = 0;
  uint8_t *e = d + len;

  for(e = d+len; d < e; ++d) {
    ret += *d;
  }

  return -ret;

}

static int
year(
    unsigned int year) {
  return year - 1990;
}

static int
manid(
    edid_manid_t *manid,
    const char *str) {

  __label__ end, err;
  int ret = 0;
  int i;
  uint8_t a1;

  for(i=0; i<3; ++i) {
    if ( (str[i] < 'A') || (str[i] > 'Z') ) {
      goto err;
    }
  }

  if ( str[3] != 0 ) {
    goto err;
  }

  manid->a0 = str[0] - 'A' + 1;
  a1        = str[1] - 'A' + 1;
  manid->a1l= a1;
  manid->a1h= a1 >> 3;
  manid->a2 = str[2] - 'A' + 1;

end:
  return ret;

err:
  printf("invalid manufacturer id '%s'\n", str);
  ret = 1;
  goto end;
}

static uint16_t
f2i(
    float f) {

  int ret = 0;
  int i;
  float c = 0.5;
  for(i=0; i<10; ++i, c *= 0.5) {
    ret <<= 1;
    if ( (f > c) || (fabsf(f - c) < 0.0009765625) ) {
      ret |= 1;
      f -= c;
    }
  }
  return ret;
}

static void
chroma(
    edid_chroma_t *chroma,
    float red_x,   float red_y,
    float green_x, float green_y,
    float blue_x,  float blue_y,
    float white_x, float white_y) {

  uint16_t i;

#define seti(name) \
    i = f2i(name); \
    chroma->name ## _low  = i; \
    chroma->name ## _high = i >> 2;
    
  seti(red_x);
  seti(red_y);
  seti(green_x);
  seti(green_y);
  seti(blue_x);
  seti(blue_y);
  seti(white_x);
  seti(white_y);
  
#undef seti

}

static uint8_t
timing_width(
    uint32_t width) {

  return width / 8 - 31;

}

static int
init_edid(
    edid_t *edid) {

  __label__ end;
  int ret;
  /* define your EDID here */

  memcpy(edid->header.magick, edid_magick, sizeof(edid->header.magick));

  ret = manid(&(edid->header.manid), "NKL");
  if ( ret ) goto end;

  edid->header.prodid = 1;
  edid->header.serial = 1;
  edid->header.week   = 17;
  edid->header.year   = year(2014);

  edid->header.ver = 1;
  edid->header.rev = 3;

  edid->display.param.digital_input     = 1;
  edid->display.param.serrated_on_green = 1;

  static const float monitor_width = 43.3;
  static const float monitor_height= 27.1;

  edid->display.width = monitor_width;
  edid->display.height= monitor_height;

  edid->display.gamma = gam(2.20);

  edid->display.features.standby   = 1;
  edid->display.features.suspend   = 1;
  edid->display.features.active_off= 1;
  edid->display.features.type      = 1;
  edid->display.features.srgb      = 0;
  edid->display.features.def_timing= 1;
  edid->display.features.def_gtf   = 0;

  chroma(
      &(edid->chroma),
      /* red   */ 0.6425781250, 0.3320312500,
      /* green */ 0.2900390625, 0.6103515625,
      /* blue  */ 0.1435546875, 0.0781250000,
      /* white */ 0.3134765625, 0.3291015625);

  edid->timing_bitmap.m720x400_70 = 1;
  edid->timing_bitmap.m640x480_75 = 1;
  edid->timing_bitmap.m640x480_72 = 1;
  edid->timing_bitmap.m640x480_60 = 1;
  edid->timing_bitmap.m800x600_60 = 1;
  edid->timing_bitmap.m800x600_72 = 1;
  edid->timing_bitmap.m800x600_75 = 1;
  edid->timing_bitmap.m1024x768_60 = 1;
  edid->timing_bitmap.m1024x768_70 = 1;
  edid->timing_bitmap.m1024x768_75 = 1;
  edid->timing_bitmap.m1280x1024_75 = 1;

  edid->timing[0].width  = timing_width(1680);
  edid->timing[0].aspect = r16_10;
  edid->timing[0].freq   = 60 - 60;

  edid->timing[1].width  = timing_width(1680);
  edid->timing[1].aspect = r16_10;
  edid->timing[1].freq   = 70 - 60;

  edid->timing[2].width  = timing_width(1680);
  edid->timing[2].aspect = r16_10;
  edid->timing[2].freq   = 75 - 60;

  edid->timing[3].width  = timing_width(1440);
  edid->timing[3].aspect = r16_10;
  edid->timing[3].freq   = 60 - 60;

  edid->timing[4].width  = timing_width(1440);
  edid->timing[4].aspect = r16_10;
  edid->timing[4].freq   = 70 - 60;

  edid->timing[5].width  = timing_width(1440);
  edid->timing[5].aspect = r16_10;
  edid->timing[5].freq   = 75 - 60;

  edid->timing[6].width  = timing_width(1440);
  edid->timing[6].aspect = r4_3;
  edid->timing[6].freq   = 60 - 60;

  edid->timing[7].width  = timing_width(1440);
  edid->timing[7].aspect = r4_3;
  edid->timing[7].freq   = 70 - 60;

  edid->dettiming.pixel_clock   = 14625;

  edid->dettiming.hor_act_low    = 1680;
  edid->dettiming.hor_act_high   = 1680 >> 8;

  edid->dettiming.hor_blank_low  = 560;
  edid->dettiming.hor_blank_high = 560 >> 8;

  edid->dettiming.hor_sync_offset_low = 104;
  edid->dettiming.hor_sync_offset_high= 104 >> 8;

  edid->dettiming.hor_sync_pulse_low = 176;
  edid->dettiming.hor_sync_pulse_high= 176 >> 8;

  edid->dettiming.vert_act_low   = 1050;
  edid->dettiming.vert_act_high  = 1050 >> 8;

  edid->dettiming.vert_blank_low = 39;
  edid->dettiming.vert_blank_high= 39 >> 8;

  edid->dettiming.vert_sync_offset_low = 3;
  edid->dettiming.vert_sync_offset_high= 3 >> 4;

  edid->dettiming.vert_sync_pulse_low = 6;
  edid->dettiming.vert_sync_pulse_high= 6 >> 4;

  uint16_t i = monitor_width * 10;
  edid->dettiming.image_width_low  = i;
  edid->dettiming.image_width_high = i >> 8;

  i = monitor_height * 10;
  edid->dettiming.image_height_low  = i;
  edid->dettiming.image_height_high = i >> 8;

  edid->dettiming.hor_border = 0;
  edid->dettiming.vert_border= 0;

  edid->dettiming.flags =
      EDID_DETTIMING_DIGSEP  |
      EDID_STEREO_NO;

  edid->descriptor[0].monitordesc.mark1 = 0;
  edid->descriptor[0].monitordesc.mark2 = 0;
  edid->descriptor[0].monitordesc.mark3 = 0;
  edid->descriptor[0].monitordesc.type  = DESC_NAME;
  setstr(edid->descriptor[0].monitordesc.data.str, "200XW Repaired");

  edid->descriptor[1].monitordesc.mark1 = 0;
  edid->descriptor[1].monitordesc.mark2 = 0;
  edid->descriptor[1].monitordesc.mark3 = 0;
  edid->descriptor[1].monitordesc.type = DESC_SERIAL;
  setstr(edid->descriptor[1].monitordesc.data.str, "1");

  edid->descriptor[2].monitordesc.mark1 = 0;
  edid->descriptor[2].monitordesc.mark2 = 0;
  edid->descriptor[2].monitordesc.mark3 = 0;
  edid->descriptor[2].monitordesc.type = DESC_RANGE;

  edid->descriptor[2].monitordesc.data.range.vert_freq_min   = 60;
  edid->descriptor[2].monitordesc.data.range.vert_freq_max   = 75;
  edid->descriptor[2].monitordesc.data.range.hor_freq_min    = 30;
  edid->descriptor[2].monitordesc.data.range.hor_freq_max    = 95;
  edid->descriptor[2].monitordesc.data.range.pixel_clock_max = 220 / 10;

  edid->extblocks = 0;

  edid->checksum = checksum((uint8_t*)edid, sizeof(*edid) - 1);
end:
  return ret;

}

static const char *
prog;

static void
usage() {
  printf(
      "usage: %s <output file>\n",
      prog);
}

static int
write_edid(
    int    fd,
    char   *data,
    size_t len) {

  __label__ end, err;
  int ret = 0;
  size_t c;
  char *i, *e;

  for(i=data, e = data + len; i < e; i += c) {
    c = write(fd, i, e - i);
    if ( c == -1 ) {
      printf("write failed: %m\n");
      goto err;
    }
  }


end:
  return ret;
err:
  ret = 1;
  goto end;
}

static int
genedid(
    int fd) {

  __label__ end;
  int ret;
  edid_t edid = { 0 };
  ret = init_edid(&edid);
  if ( ret ) goto end;
  ret = write_edid(fd, (char *)&edid, sizeof(edid));

end:
  return ret;

}

int
main(
    int argc,
    char *argv[]) {

  __label__ end, err;
  int ret = 0;
  int fd;

  prog = argv[0];

  if ( argc < 2 ) {
    usage();
    goto err;
  }

  fd = open(argv[1], O_WRONLY | O_CREAT, 0666);

  if ( fd == -1 ) {
    printf("can't open file '%s': %m\n", argv[1]);
    goto err;
  }

  ret = genedid(fd);

  close(fd);
end:
  return ret;
err:
  ret = 1;
  goto end;

}
