PROJECT_NAME := gedid
AUTHOR := Andrej Gelenberg <andrej.gelenberg@udo.edu>
VER_MAJOR := 0
VER_MINOR := 1

CLEAN := parse genedid

all: parse genedid

parse: parse.o edid.o
genedid: genedid.o edid.o
